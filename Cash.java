public class Cash {
    double price;
    double moneyGiven;
    double change;
    String changeOutput = "";

    double getChange(double price, double moneyGiven) {

       change = moneyGiven - price;
       return change;
    }

    String getChangeOutput(double change) {
        int nchange = (int)change;
        int nmbr;

        if (nchange/1000 > 0) {
            nmbr = nchange/1000;
            nchange = nchange - nmbr * 1000;
            if (nmbr>0) changeOutput += ""+nmbr+" 1000 lapp(er)"+"\n";
        }
        if (nchange/500 > 0) {
            nmbr = nchange/500;
            nchange = nchange - nmbr * 500;
            if (nmbr>0) changeOutput += ""+nmbr+" 500 lapp(er)"+"\n";
        }
        if (nchange/200 > 0) {
            nmbr = nchange/200;
            nchange = nchange - nmbr * 200;
            if (nmbr>0) changeOutput += ""+nmbr+" 200 lapp(er)"+"\n";
        }
        if (nchange/100 >= 0) {
            nmbr = nchange/100;
            nchange = nchange - nmbr * 100;
            if (nmbr>0) changeOutput += ""+nmbr+" 100 lapp(er)"+"\n";
        }
        if (nchange/50 >= 0) {
            nmbr = nchange/50;
            nchange = nchange - nmbr * 50;
            if (nmbr>0) changeOutput += ""+nmbr+" 50 lapp(er)"+"\n";
        }
        if (nchange/20 >= 0) {
            nmbr = nchange/20;
            nchange = nchange - nmbr * 20;
            if (nmbr>0) changeOutput += ""+nmbr+" 20 krone(r)"+"\n";
        }
        if (nchange/10 >= 0) {
            nmbr = nchange/10;
            nchange = nchange - nmbr * 10;
            if (nmbr>0) changeOutput += ""+nmbr+" 10 krone(r)"+"\n";
        }
        if (nchange/5 >= 0) {
            nmbr = nchange/5;
            nchange = nchange - nmbr * 5;
            if (nmbr>0) changeOutput += ""+nmbr+" 5 krone(r)"+"\n";
        }
        if (nchange >= 0) {
            if (nchange>0) changeOutput += ""+nchange+" krone(r)"+"\n";
        }

        return changeOutput;
    }

    public Cash(double p) {
        this.price = p;
    }

    public Cash() {
        this.price = 0;
    }
}
