import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("HELP: java Main 'amount to pay'");
            System.exit(0);
        }

        CreditCard cc = new CreditCard();
        SavingsCard sc = new SavingsCard();
        Cash c = new Cash();

        double price = Double.parseDouble(args[0]);

        Scanner input = new Scanner(System.in);

        System.out.println("How would you like to pay?");
        System.out.println("1: cash");
        System.out.println("2: credit card");
        System.out.println("3: savings card");

        int payMethod = input.nextInt();
        double newBalance;

        switch (payMethod) {
            case 1: // cash

                System.out.println("how much will you pay with?");
                String lol = input.next();
                double money = Double.parseDouble(lol);
                double yourChange = c.getChange(price, money);
                System.out.println("money back: "+yourChange);
                String yourChangeOutput = c.getChangeOutput(yourChange);
                System.out.println("Here is your change:"+"\n"+yourChangeOutput);
                break;

            case 2: // credit card

                newBalance = cc.pay(price);
                System.out.println("balance: "+newBalance);
                break;

            case 3: // savings card

                newBalance = sc.pay(price);
                System.out.println("balance: "+ newBalance);
                break;

            default:

                System.out.println("Invalid payment method.");
                break;
        }
    }
}
