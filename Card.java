public abstract class Card {
    double balance;

    double pay(double price) {
        this.balance -= price;
        return balance;
    }

    public Card(double b){
        this.balance = b;
    }

    public Card(){
        balance = 0;
    }
}
